<?php
session_start();
require ('Database.php');

// Utiliser la connexion à la base de données pour exécuter des requêtes SQL...
// Vérifier si le formulaire a été soumis
if (isset($_POST['submit'])) {
    // Récupérer les données du formulaire
    $pseudo = $_POST['pseudo'];
    $mdp = $_POST['mdp'];
    $email = $_POST['email'];

    // Vérifier si le pseudo est déjà utilisé
    $sql = "SELECT * FROM Utilisateur WHERE Pseudo = ?";
    $stmt = $bdd->prepare($sql);
    $stmt->execute([$pseudo]);
    $result = $stmt->fetch();
    if ($result) {
        echo "<p>Le pseudo est déjà utilisé. Veuillez choisir un autre pseudo.</p>";
    } else {
        // Insérer les données dans la base de données
        $sql = "INSERT INTO Utilisateur (Pseudo, Mdp, Email, Date_connection)
                VALUES (?, ?, ?, NOW())";
        $stmt = $bdd->prepare($sql);
        $stmt->execute([$pseudo, $mdp, $email]);
        echo "<p>Inscription réussie. Vous pouvez maintenant vous connecter.</p>";
        header('Location: login.php');
        exit();
    }
    // Fermer la connexion
    $bdd = null;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    <title>Inscription</title>
    <html>
    <body>

<h2>Inscription</h2>

<form method="post" action="#">
    Pseudo: <input type="text" name="pseudo"><br>
    Mot de passe: <input type="password" name="mdp"><br>
    Email: <input type="text" name="email"><br>
    <input type="submit" name="submit" value="Submit" />
</form>

</body>
</html>