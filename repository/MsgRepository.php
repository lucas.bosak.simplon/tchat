<?php
require 'Database.php';
class RepositoryMessage {
  private $pdo;
  public function __construct() {
    $dsn = 'mysql:host=localhost;dbname=Tchat';
    $utilisateur = 'Tchat';
    $motDePasse = 'Tchat';
      $db = new PDO($dsn, $utilisateur, $motDePasse);
      $this->pdo = $db;
  }

  public function getIfMessages() {
      $query = "SELECT * FROM tchat";
      $stmt = $this->pdo->query($query);

      $messages = array();

      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          $message = array(
              'Id_message' => $row['Id_message'],
              'Pseudo' => $row['Pseudo'],
              'Msg' => $row['Msg'],
              'Dates' => $row['Dates']
          );

          $messages[] = $message;
      }

      return $messages;
  }
}

$repository = new RepositoryMessage();
$messages = $repository->getIfMessages();







?>