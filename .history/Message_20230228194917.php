<?php
require 'repository/MsgRepository.php';

class Message {
  private $repository;

  public function __construct() {
    $this->repository = new RepositoryMessage();
  }

  public function generateMessages() {
    $messages = $this->repository->getIfMessages();
    foreach ($messages as $message) {
    if ($message['Pseudo'] == $_SESSION['user']) {
        $pseudo = $message['Pseudo'];
        $msg = $message['Msg'];
        $date = $message['Dates'];
  
        // Generate the message with the desired formatting
        echo '<div class="M M_envoye">' .
             '<span class="pseudo">' . $pseudo . '</span><br>  ' .
             '<span class="M_date">' . $date . '</span><br>' .
             '<p class="M_message">' . $msg . '</p>' .
             '</div>';
      
    } else {
      $pseudo = $message['Pseudo'];
      $msg = $message['Msg'];
      $date = $message['Dates'];

      // Generate the message with the desired formatting
      echo '<div class="M M_recu">' .
           '<span class="pseudo">' . $pseudo . '</span><br>' .
           '<span class="M_date">' . $date . '</span><br>' .
           '<p class="M_message">' . $msg . '</p>' .
           '</div>';
      }
    }
  }


}