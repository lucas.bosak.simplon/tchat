<?php
require 'repository/MsgRepository.php';

class Message {
  private $repository;

  public function __construct() {
    $this->repository = new RepositoryMessage();
  }

  public function generateMessages() {
    $messages = $this->repository->getIfMessages();
    if (isset($_SESSION['user'])) {
      foreach ($messages as $message) {
        $pseudo = $message['Pseudo'];
        $msg = $message['Msg'];
        $date = $message['Dates'];
  
        // Generate the message with the desired formatting
        echo '<div class="message">' .
             '<span class="pseudo">' . $pseudo . '</span><br>' .
             '<span class="M_date">' . $date . '</span><br>' .
             '<p class="msg">' . $msg . '</p>' .
             '</div>';
      }
    } else {
      echo "nop";
    }
  }
  