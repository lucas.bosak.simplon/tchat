<?php
session_start();
require 'Database.php';

// Vérifier si le formulaire est soumis
if (isset($_POST['submit']) && $_POST['login_pseudo'] && $_POST['login_mdp'] ) {
    
    $pseudo = $_POST['login_pseudo'];
    $stmt = $bdd->prepare("SELECT * FROM utilisateur WHERE Pseudo = :pseudo");
    $stmt->execute(['pseudo' => $pseudo]);

    if ($stmt->rowCount() > 0) {
        $user = $stmt->fetch();

        if (($_POST['login_mdp'] === $user['Mdp'])) {
           $_SESSION['user'] = $user['Pseudo'];
            echo "Connexion réussie";
            header('Location: index.php');
            exit();
        } else {
            echo "Connexion Error";
        }
    }else{
        echo "Connexion Error";  
    } 
    } else {
        echo "Merci de rensiger le Pseudonyme et le Mot de passe";
}
//$db = NULL;
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="../Css/style_login.css">
</head>
<body>
    <div class="login" id="login">
        <img src="..\img\logo.png" alt="logo" id="logo">
        <form action="#" method="post">
            <label for="pseudo">Pseudonyme <br></label>
            <input type="text" name="login_pseudo" id="pseudo" class="pseudo" >
            <label for="mdp"><br>Mot de passe<br></label>
            <input type="password" name="login_mdp" ><br>
            <input type="checkbox" name="login_remember" id="remember">
            <label for="remember">Se rappeler de moi <br></label>
            <a href="#" class="forget" id="forget">Mot de passe oublié ?<br></a>
            <input type="submit" name="submit" value="Login" class="btn_login" id="login">
            <p>OU</p>
        </form>
    </div>

    <input type="button" value="Inscription" onclick="window.location.href='inscription.php';" class="btn_inscription2" id="btn_inscription2"></button>
</body>
</html>
