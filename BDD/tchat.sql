-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 28 fév. 2023 à 19:06
-- Version du serveur : 5.7.40
-- Version de PHP : 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `tchat`
--

-- --------------------------------------------------------

--
-- Structure de la table `lire`
--

DROP TABLE IF EXISTS `lire`;
CREATE TABLE IF NOT EXISTS `lire` (
  `Id_message` int(11) NOT NULL,
  `Id` int(11) NOT NULL,
  PRIMARY KEY (`Id_message`,`Id`),
  KEY `Lire_Utilisateur0_FK` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `lire`
--
ALTER TABLE `lire`
  ADD CONSTRAINT `Lire_Utilisateur0_FK` FOREIGN KEY (`Id`) REFERENCES `utilisateur` (`Id`),
  ADD CONSTRAINT `Lire_tchat_FK` FOREIGN KEY (`Id_message`) REFERENCES `tchat` (`Id_message`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
